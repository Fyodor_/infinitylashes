$(document).ready(function () {
  /* academy page -> show - hide button */
  $('.academy-page__button-wrapper .button').on('click', function () {
    $('.academy-page__adaptive').toggleClass('academy-page__opened');
    $('.academy-page__button-wrapper .button-svg').toggleClass('opened');
    // $(function () {
    //   $("a.button").click(function () {
    //     var _href = $(this).attr("href");
    //     $("html, body").animate({
    //       scrollTop: $(_href).offset().top + "px"
    //     }, 500);
    //     return false;
    //   });
    // });
  });
});