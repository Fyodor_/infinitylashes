$(document).ready(function () {
  /* ============================================================================
   * show - hide sort
   * ============================================================================ */
  $(document).mouseup(function (e) { // событие клика по веб-документу
    var div = $(".shop-page__sort-by"); // тут указываем ID элемента
    var div2 = $(".sort-variants"); // тут указываем ID элемента
    if (!div.is(e.target) // если клик был не по нашему блоку
      &&
      div.has(e.target).length === 0 // и не по его дочерним элементам
      &&
      !div2.is(e.target) &&
      div2.has(e.target).length === 0
    ) {
      $('.sort-variants').removeClass("show-filters");
      $('.shop-page__sort-by').removeClass("shop-page__filter-minus");
    } else if (div.is(e.target)) {
      $('.sort-variants').toggleClass("show-filters");
      $('.shop-page__sort-by').toggleClass("shop-page__filter-minus");
    }
  });
  $('body').on('click', '.sort-variants a', function (event) {
    var newText = $(this).text();
    $('.shop-page__sort-by').text(newText);
    $('.sort-variants').removeClass("show-filters");
    $('.shop-page__sort-by').removeClass("shop-page__filter-minus");
    event.preventDefault();
  });
  $('body').on('click', '.shop-page__category-adaptive', function (event) {
    $('.shop-page__category-adaptive').toggleClass("shop-page__category-adaptive-minus");
  });
});