$(document).ready(function() {
  /* ============================================================================
   * shop-item-page - Add your promotional code input
   * ============================================================================ */
  $('#cart-page__select-title').click(function() {
    $('#cart-page__promotional-input').slideToggle({
      duration: 400,
      start: function() {
        $('.promotion-stroke').toggleClass('promotion-stroke-move');
      }
    });
  });

  /* крестик при вводе */
  $('#cart-page__promotional-input input').keyup(function() {
    // var value1 = $('#adaptive-search-input').val();
    // console.log(value1.length);
    //var val2 = document.getElementById('adaptive-search-input');
    //console.log(val2.value.length);
    $('.close-search-code').css('visibility', 'visible');
  });
  $('.close-search-code').on('click', function() {
    $('#cart-page__promotional-input input').val('');
    $('.close-search-code').css('visibility', 'hidden');
    $('#cart-page__promotional-input input').focus();
  });
});
