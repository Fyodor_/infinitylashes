$(document).ready(function () {
  /* ^> on careers accordeon */
  $('#accordionCareers .collapse').on('show.bs.collapse', function (event) {
    $(event.target).parent().find('.faq-arrow-new').addClass('faq-arrow-look-up');
  });
  $('#accordionCareers .collapse').on('hide.bs.collapse', function (event) {
    $(event.target).parent().find('.faq-arrow-new').removeClass('faq-arrow-look-up');
  });
});