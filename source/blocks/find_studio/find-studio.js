/* ============================================================================
 * ключ maps api ДЛЯ ТЕСТОВОГО САЙТА - AIzaSyAioiyPpDqMuUnz7g7mKiqX4BCtg6bqXSU
 * ============================================================================ */
$(document).ready(function () {
  /* ============================================================================
   * переместим поиск при маленьком экране
   * ============================================================================ */
  function changeSearchCityPosition() {
    if ($(window).width() < 767) {
      $('.city-search').appendTo($('.find-studio__adaptive-find'));
    } else {
      $('.city-search').appendTo($('#city-search'));
    }
  }

  changeSearchCityPosition();

  $(window).resize(
    function () {
      changeSearchCityPosition();
    }
  );
})

/* ============================================================================
 * google maps api
 * ============================================================================ */

// var myMap;
// var marker;
// var num = 0;

// window.initMap = function() {
//   myMap = new google.maps.Map(document.getElementById('map'), {
//     center: {
//       lat: 51.511112,
//       lng: 7.474627
//     },
//     zoom: 10
//   });


//   function addMarker(coordinates) {
//     marker = new google.maps.Marker({
//       position: coordinates,
//       map: myMap,
//       icon: '../img/map-pin.png',
//       animation: google.maps.Animation.DROP,
//     });
//   }


//   var markersArray = [{
//       lat: 51.511112,
//       lng: 7.474627
//     },
//     {
//       lat: 51.451854,
//       lng: 7.012553
//     },
//     {
//       lat: 51.387965,
//       lng: 7.782429
//     },
//     {
//       lat: 51.559913,
//       lng: 7.731702
//     },
//     {
//       lat: 51.454293,
//       lng: 7.357090
//     },
//   ]

//   for (var i = 0; i < markersArray.length; i++) {
//     addMarker(markersArray[i]);
//   }

// }



/* ============================================================================
 * ie version - last working wersion - removed because now get from backend
 * ============================================================================ */
//TODO add this code fore showing map without back
// if ($('body').hasClass('find-studio-page')) {
//   var points = [{
//       lat: 51.511112,
//       lng: 7.474627
//     },
//     {
//       lat: 51.451854,
//       lng: 7.012553
//     },
//     {
//       lat: 51.387965,
//       lng: 7.782429
//     },
//   ];

//   var markers = [];
//   var map;
//   var InfoWindow1;
//   var InfoWindow2;
//   var InfoWindow3;

//   function initMap() {
//     map = new google.maps.Map(document.getElementById('map'), {
//       zoom: 10,
//       center: points[0],
//     });
//   };

//   function drop() {
//     clearMarkers();
//     for (var i = 0; i < points.length; i++) {
//       addMarkerWithTimeout(points[i], i * 200);
//     }
//   }

//   function addMarkerWithTimeout(position, timeout) {
//     window.setTimeout(function () {
//       markers.push(new google.maps.Marker({
//         position: position,
//         map: map,
//         animation: google.maps.Animation.DROP,
//         icon: '../img/map-pin.png',
//       }));

//       InfoWindow1 = new google.maps.InfoWindow({
//         content: '<div id="studio1"><h6>Infinity Lashes Berlin</h6><br>Tauentzienstrasse, 78/ 24 <br>Berlin, 10779, Germany<br>T:+49.30.21.93.70.58</div>',
//       });

//       InfoWindow2 = new google.maps.InfoWindow({
//         content: '<div id="studio2"><h6>Baby Lash</h6><br>Tauentzienstrasse, 78/ 24 <br>Berlin, 10779, Germany<br>T:+49.30.21.93.70.58</div>',
//       });

//       InfoWindow3 = new google.maps.InfoWindow({
//         content: '<div id="studio3"><h6>Amore Lash</h6><br>Tauentzienstrasse, 78/ 24 <br>Berlin, 10779, Germany<br>T:+49.30.21.93.70.58</div>',
//       });

//     }, timeout);
//   }

//   function clearMarkers() {
//     for (var i = 0; i < markers.length; i++) {
//       markers[i].setMap(null);
//     }
//     markers = [];
//   }


//   setTimeout(function () {
//       drop();
//     },
//     2000);


//   setTimeout(function () {
//       markers[0].addListener('click', function () {
//         InfoWindow1.open(map, markers[0])
//       })
//       markers[1].addListener('click', function () {
//         InfoWindow2.open(map, markers[1])
//       })
//       markers[2].addListener('click', function () {
//         InfoWindow3.open(map, markers[2])
//       })
//     },
//     5000);

//   $('.city-search-items-wrapper .item1, #studio1').on("click", function () {
//     map.setZoom(18);
//     map.setCenter(markers[0].getPosition());
//   });
//   $('.city-search-items-wrapper .item2').on("click", function () {
//     map.setZoom(18);
//     map.setCenter(markers[1].getPosition());
//   });
//   $('.city-search-items-wrapper .item3').on("click", function () {
//     map.setZoom(18);
//     map.setCenter(markers[2].getPosition());
//   });
// }