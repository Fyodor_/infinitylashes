$(document).ready(function () {
  /* ============================================================================
   * hover-main-menu
   * ============================================================================ */
  $('header#top nav li a').hover(
    function (event) {
      //if (event.target.classList[0] === 'header__academy' || event.target.classList[0] === 'header__shop') {
      if (event.target.classList[0] === 'header__shop') {
        $('.header__hover-menu .row').removeClass('header-hover-show');
        $('#nav-line').addClass('header__hover-header-no-shadow');
        $('.header__hover-menu .row').each(function (i, elem) {
          if ($(this).hasClass(event.target.classList[0])) {
            $(this).addClass('header-hover-show');
          }
          $('.header__hover-menu').addClass('header__hover-menu-active');
          $('header#top').removeClass('header-shadow');
          $("#nav-line").addClass('header__hover-header-no-shadow');
        })
      }
    },
    function (event) {
      if ($(event.relatedTarget).hasClass('header__hover-menu-active')) {
        return
      //} else if ($(event.relatedTarget).hasClass('header__shop') || $(event.relatedTarget).hasClass('header__academy')) {
      } else if ($(event.relatedTarget).hasClass('header__shop')) {
      } else {
        $('.header__hover-menu').removeClass('header__hover-menu-active');
        $('#nav-line').removeClass('header__hover-header-no-shadow');
        $('header#top').addClass('header-shadow');
      }
    }
  );
  //#nav-line was #top
  $('.header__hover-menu').hover(
    function () {},
    function (event) {
      if ($(event.relatedTarget).hasClass('header__shop' || 'header__academy')) {} else {
        $('.header__hover-menu').removeClass('header__hover-menu-active');
        $("#nav-line").removeClass('header__hover-header-no-shadow');
        $('header#top').addClass('header-shadow');
      }
    }
  );
  /* ============================================================================
   * change photos in academy hover menu
   * ============================================================================ */
  $('.academy-hover li a').hover(
    function (event) {
      $('.header__hover-img').find('img').removeClass('header__hover-img-active');
      $('.header__hover-img img').each(function (i, elem) {
        if ($(this).hasClass(event.target.classList[0])) {
          $(this).addClass('header__hover-img-active');
        }
      })
    },
    function () {}
  )
});