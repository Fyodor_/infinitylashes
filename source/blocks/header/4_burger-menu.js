$(document).ready(function () {
  /*burger*/
  $('.header__boorger-wrapper').on('click', function () {
    if ($('.header__burger-menu-wrapper').hasClass('header__burger-menu-wrapper-active')) {
      $('.burger-menu__second-window').removeClass('activeSecondWindow');
      $('.burger-menu__third-window').removeClass('activeThirdWindow');
      $('.burger-menu__third-window').css({'left':'-150%', 'right':'auto'});
      $('.burger-menu__second-window').css({'left':'-150%', 'right':'auto'});

      setTimeout(function () {
        $('body').css('height', 'auto');
        $("body").removeClass("modal-open");
      }, 300);

    }
    $('.header__boorger').toggleClass('boorger-animate');
    $('.header__boorger-wrapper').toggleClass('boorger-wrapper-animate');
    $('.header__burger-menu-wrapper').toggleClass('header__burger-menu-wrapper-active');
    $('body').toggleClass('body-header__burger-menu-wrapper-active');
    $('body').css('height', '100%');
    $("body").addClass("modal-open");
  });
  /*add - remove -> (if is it inside menu show)*/
  var dataFirstAttr = $('.burger-menu__first-window a[data-attr]');
  var dataSecondAttr = $('.burger-menu__second-window[data-parent-attr]');
  dataFirstAttr.each(
    function (index, element) {
      dataSecondAttr.each(function (index2, element2) {
        if (element.dataset.attr === element2.dataset.parentAttr) {
          $(element).css('display', 'block');
        }
      })
    }
  );
  var dataFirstAttr = $('.burger-menu__second-window a[data-attr]');
  var dataSecondAttr = $('.burger-menu__third-window[data-parent-attr]');
  dataFirstAttr.each(
    function (index, element) {
      dataSecondAttr.each(function (index2, element2) {
        if (element.dataset.attr === element2.dataset.parentAttr) {
          $(element).css('display', 'block');
        }
      })
    }
  );
  /*go to next*/
  var upperMenu = 'Menu';
  $('.burger-menu__first-window .burger-menu-punkts a').on('click', function (event) {
    var thiss = this.dataset.attr;
    var prevHeight = $(this).closest('.burger-menu-wrapper').height();
    var dataSecondAttr = $('.burger-menu__second-window[data-parent-attr]');
    dataSecondAttr.each(function (index2, element2) {
      if (thiss === element2.dataset.parentAttr) {
        $(element2).css({'left':'0', 'right':'0', 'min-height':prevHeight + 500 + 'px'});
        $(element2).addClass('activeSecondWindow');
        $('.burger-footer-wrapper').addClass('burger-footer-wrapper-hide');
      }
    });
    $('.burger-menu-submenu-name').text(thiss);
    $('.burger-back-wrapper').css('display', 'block');
    upperMenu = thiss;
  });
  $('.burger-menu__second-window .burger-menu-punkts a').on('click', function (event) {
    var thiss = this.dataset.attr;
    var prevHeight = $(this).closest('.burger-menu-wrapper').height();
    var dataSecondAttr = $('.burger-menu__third-window[data-parent-attr]');
    dataSecondAttr.each(function (index2, element2) {
      if (thiss === element2.dataset.parentAttr) {
        $(element2).css({'left':'0', 'right':'0', 'min-height':prevHeight + 500 + 'px'});
        $(element2).addClass('activeThirdWindow');
      }
    })
    $('.burger-menu-submenu-name').text(thiss);
  });
  /*go to prev*/
  $('.burger-back-wrapper').on('click', function (event) {
    var thiss = this.dataset.attr;
    $('.burger-menu__language-window').removeClass('burger-menu__language-window-show');
    if ($('.burger-menu__third-window').hasClass('activeThirdWindow')) {
      $('.burger-menu__third-window').css({'left':'-150%', 'right':'auto'});
      $('.burger-menu__third-window').removeClass('activeThirdWindow');
      $('.burger-menu-submenu-name').text(upperMenu);
    } else {
      $('.burger-menu__second-window').css({'left':'-150%', 'right':'auto'});
      $('.burger-back-wrapper').css('display', 'none');
      $('.burger-menu__second-window').removeClass('activeSecondWindow');
      $('.burger-footer-wrapper').removeClass('burger-footer-wrapper-hide');
    }
  });

  /* ============================================================================
   * language change
   * ============================================================================ */
  $('.burger-menu__language-menu-trigger').on('click', function () {
    $('.burger-menu__language-window').toggleClass('burger-menu__language-window-show');
    $('.burger-footer-wrapper').addClass('burger-footer-wrapper-hide');
    $('.burger-back-wrapper').css('display', 'block');
  })
});