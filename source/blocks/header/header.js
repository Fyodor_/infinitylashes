$(document).ready(function () {
  if (document.documentMode || /Edge/.test(navigator.userAgent)) {
    (function badBrowser() {
      $('header .main .main-search button').css({
        'top': '2px',
        'right': '0px'
      });
    })();
  }
  /*main page - search size change*/
  $('.search-adaptive-left, .search-adaptive-right').on('click', function () {
    $('.header__adaptive-search').toggleClass('header__adaptive-search-active');
    if ($('.header__adaptive-search').hasClass('header__adaptive-search-active')) {
      setTimeout(
        function () {
          $('#adaptive-search-input').focus();
        },
        400
      )
    }
  });
  /* крестик при вводе */
  $('#adaptive-search-input').keyup(function () {
    // var value1 = $('#adaptive-search-input').val();
    // console.log(value1.length);
    //var val2 = document.getElementById('adaptive-search-input');
    //console.log(val2.value.length);
    $('.close-search').css("visibility", "visible");
  });
  $('.close-search').on('click', function () {
    $('#adaptive-search-input').val('');
    $('.close-search').css("visibility", "hidden");
    $('#adaptive-search-input').focus();
  });
  /* hide when click outside */
  $(document).mouseup(function (e) { // событие клика по веб-документу
    var div = $(".header__adaptive-search"); // тут указываем ID элемента
    if (!div.is(e.target) // если клик был не по нашему блоку
      &&
      div.has(e.target).length === 0 // и не по его дочерним элементам
    ) {
      $('.header__adaptive-search').removeClass("header__adaptive-search-active");
    }
  });

  //переключение языка
  $(".change-language-button").on('click', function () {
    $('.dropdown-menu-change').toggleClass('dropdown-menu-change-visible');
  });

  /* hide when click outside */
  $(document).mouseup(function (e) { // событие клика по веб-документу
    var div = $(".dropdown-menu-change"); // тут указываем ID элемента
    if (!div.is(e.target) // если клик был не по нашему блоку
      &&
      div.has(e.target).length === 0 // и не по его дочерним элементам
      &&
      !$('.change-language-button').is(e.target) //кнопка на которую нажимают для открытия
      &&
      $('.change-language-button').has(e.target).length === 0
    ) {
      $('.dropdown-menu-change').removeClass("dropdown-menu-change-visible");
    }
  });
});