$(document).ready(function() {
  /* ============================================================================
   * cart hover
   * ============================================================================ */
  var cartHoverTimeout;
  function addTimoutCart() {
    cartHoverTimeout = setTimeout(function() {
      $('.cart-hover-menu').removeClass('show-cart-hover-menu');
    }, 3000);
  }
  function removeTimoutCart() {
    clearTimeout(cartHoverTimeout);
  }

  $('.main-enter__basket').mouseenter(function() {
    $('.cart-hover-menu').addClass('show-cart-hover-menu');
  });

  $('.main-enter__basket').mouseleave(function() {
    addTimoutCart();
  });

  $('.cart-hover-menu').mouseenter(function() {
    removeTimoutCart();
  });
  $('.cart-hover-menu').mouseleave(function() {
    addTimoutCart();
  });
  /* ============================================================================
   * show persents of free sheeping - in cart hover
   * ============================================================================ */
  $('.progress-bar-value').text($('.cart-hover-menu .progress-bar').attr('aria-valuenow') + ' %');
});
