$(document).ready(function() {
  $('.cart-page__set-details-header').on('click', function() {
    $(this).parent().find('.cart-page__set-details-body').toggleClass('cart-page__set-details-body-open');
    $(this).parent().find('.cart-page__set-details-body').slideToggle({
      duration: 400,
      start: function() {
        $(this).parent().find('.more-set-info-stroke').toggleClass('more-set-info-stroke-move');
      }
    });
  });
});
