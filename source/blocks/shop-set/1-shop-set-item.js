$(document).ready(function() {
  /* ============================================================================
   * shop-set-page - set item slider
   * ============================================================================ */
  if ($('body').hasClass('shop-set-page')) {
    /* переделано для бека, чтобы можно было обратиться */
    if (!document.lightsliderOnItem) {
      document.lightsliderOnItem = 0;
    }

    function itemSlider1() {
      var verticalHorizontal = true;
      if ($(window).width() < 1200) {
        verticalHorizontal = false;
      }

      $(window).on('resize', function() {
        document.lightsliderOnItem.refresh();
      });

      //https://github.com/sachinchoolur/lightslider/issues/61
      //https://stackoverflow.com/questions/46421110/light-slider-refresh-not-working
      //$('#shop-item-page__item-slider').goToNextSlide();
      document.lightsliderOnItem = $(document).find('#shop-item-page__item-slider').lightSlider({
        gallery: true,
        item: 1,
        vertical: verticalHorizontal,
        verticalHeight: 430, //555
        vThumbWidth: 100,
        thumbItem: 4,
        thumbMargin: 10,
        slideMargin: 0,
        galleryMargin: 70,
        adaptiveHeight: true,
        loop: true,
        controls: true,
        responsive: [
          {
            breakpoint: 1199,
            settings: {
              thumbItem: 4,
              thumbMargin: 45
            }
          },
          {
            breakpoint: 992,
            settings: {
              thumbItem: 0,
              gallery: false,
              adaptiveHeight: true,
              galleryMargin: 30
            }
          },
          {
            breakpoint: 400,
            settings: {
              verticalHeight: 300,
              gallery: false,
              adaptiveHeight: true
            }
          },
          {
            breakpoint: 0,
            settings: {
              adaptiveHeight: true
            }
          }
        ],
        onSliderLoad: function(el) {
          checkNumOfItems1();
          changeRowsContainer1();
        }
      });
    }
    var i = 0;
    itemSlider1();
    $(window).resize(function() {
      document.lightsliderOnItem.destroy();
      itemSlider1();
    });
    /*fix one image controls hide*/
    function checkNumOfItems1() {
        if ($('#shop-item-page__item-slider.first-page-slider li').length > 1) {
          if (i == 0) {
            i++;
            document.lightsliderOnItem.refresh();
            $('.first-page-slider').css('visibility', 'visible');
          }
        } else {
          $('.first-page-slider').css('visibility', 'visible');
        }
      
    }
    /*change rows container*/
    function changeRowsContainer1() {
      $('.first-page-slider > .lSAction').remove();

      $('.first-page-slider .lSAction').appendTo('.first-page-slider');
      $('.lSAction').css('opacity', 1);
    }

    /* ============================================================================
     * shop-item-page - item zoom
     * ============================================================================ */
    var outOne = $('.shop-item-page__zoom-here');
    if ($(window).width() > 991) {
      $('.shop-item-page__zoom').parent().zoom({
        target: outOne,
        magnify: 2,
        onZoomIn: function() {
          $('.shop-item-page__zoom-here').css('z-index', '1');
        },
        onZoomOut: function() {
          $('.shop-item-page__zoom-here').css('z-index', '0');
        }
      });
    }
    /* for second slider */
    var outTwo = $('.shop-item-page__zoom-here-two');
    if ($(window).width() > 991) {
      $('.shop-item-page__zoom-two').parent().zoom({
        target: outTwo,
        magnify: 2,
        onZoomIn: function() {
          $('.shop-item-page__zoom-here-two').css('z-index', '1');
        },
        onZoomOut: function() {
          $('.shop-item-page__zoom-here-two').css('z-index', '0');
        }
      });
    }
    /* ============================================================================
     * shop-item-page - stars rating
     * ============================================================================ */
    $(function() {
      $('.star-ratings').barrating({
        theme: 'css-stars',
        readonly: true
      });
      $('.star-ratings-change').barrating({
        theme: 'css-stars',
        readonly: false
      });
    });
  }
  /* ============================================================================
   * shop-item-page - quantiti-of-order
   * ============================================================================ */
  $('.shop-set-page .shop-item-page__quantiti-of-order .minus').click(function() {
    var valNow = $('#shop-item-page__quantiti-of-order');
    val = parseInt(valNow.val()) - 1;
    val = val < 1 ? 1 : val;
    valNow.val(val);
    inputPosition();
    return false;
  });
  $('.shop-set-page .shop-item-page__quantiti-of-order .plus').click(function() {
    var valNow = $('#shop-item-page__quantiti-of-order');
    val = parseInt(valNow.val()) + 1;
    valNow.val(val);
    inputPosition();
    return false;
  });

  if ($('.label-wrapper').length > 0) {
    inputPosition();
  }

  /* ============================================================================
   * change buttons on shop item
   * ============================================================================ */
  // $('body').on('click', '.shop-set-page .shop-item-page__choose-block', function(event) {
  //   if ($(this).hasClass('shop-item-page__choose-block-disabled')) {
  //     event.preventDefault();
  //   } else {
  //     if ($(this).hasClass('shop-item-page__choose-block-active')) {
  //       $(this).removeClass('shop-item-page__choose-block-active');
  //       event.preventDefault();
  //     } else {
  //       $(this)
  //         .parent()
  //         .parent()
  //         .find('.shop-item-page__choose-block')
  //         .removeClass('shop-item-page__choose-block-active');
  //       $(this).addClass('shop-item-page__choose-block-active');
  //       event.preventDefault();
  //     }
  //   }
  // });
  /* ============================================================================
   * show cart-hover-menu on add to bag button
   * ============================================================================ */
  $('.shop-item-page__button-for-order a').on('click', function(event) {
    $('.cart-hover-menu').addClass('show-cart-hover-menu');
    setTimeout(function() {
      $('.cart-hover-menu').removeClass('show-cart-hover-menu');
    }, 1500);
    if ($(window).width() < 991) {
      event.preventDefault();
      $('.added-to-cart-wrapper').css('right', '25px');
      setTimeout(function() {
        $('.added-to-cart-wrapper').css('right', '-100%');
      }, 3000);
    }
  });
});
/* ============================================================================
 * lenght of quantity when changed numders
 * ============================================================================ */
function inputPosition() {
  var inputs = $('.label-wrapper input');
  for (var i = 0; i < inputs.length; i++) {
    inputPositionChange(inputs[i]);
  }
}
function inputPositionChange(input) {
  if ($(input).val().length < 2) {
    $(input).css('margin-left', '60%');
  }
  if ($(input).val().length >= 2) {
    $(input).css('margin-left', '55%');
  }
  if ($(input).val().length >= 3) {
    $(input).css('margin-left', '50%');
  }
}

/* ============================================================================
   * second slider on sets page
   * ============================================================================ */
if ($('body').hasClass('shop-set-page')) {
  if (!document.lightsliderOnItem2) {
    document.lightsliderOnItem2 = 0;
  }

  function itemSlider2() {
    var verticalHorizontal = true;
    if ($(window).width() < 1200) {
      verticalHorizontal = false;
    }

    $(window).on('resize', function() {
      document.lightsliderOnItem2.refresh();
    });

    document.lightsliderOnItem2 = $(document).find('#shop-item-page__item-slider2').lightSlider({
      gallery: true,
      item: 1,
      vertical: verticalHorizontal,
      verticalHeight: 430, //555
      vThumbWidth: 100,
      thumbItem: 4,
      thumbMargin: 10,
      slideMargin: 0,
      galleryMargin: 70,
      adaptiveHeight: true,
      loop: true,
      controls: true,
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            thumbItem: 4,
            thumbMargin: 45
          }
        },
        {
          breakpoint: 992,
          settings: {
            thumbItem: 0,
            gallery: false,
            adaptiveHeight: true,
            galleryMargin: 30
          }
        },
        {
          breakpoint: 450,
          settings: {
            verticalHeight: 300,
            gallery: false,
            adaptiveHeight: true
          }
        },
        {
          breakpoint: 0,
          settings: {
            adaptiveHeight: true
          }
        }
      ],
      onSliderLoad: function(el) {
        checkNumOfItems2();
        changeRowsContainer2();
      }
    });
  }
  var j = 0;

  $(window).resize(function() {
    if (document.lightsliderOnItem2) {
      document.lightsliderOnItem2.destroy();
      itemSlider2();
    }
  });
  /*fix one image controls hide*/
  function checkNumOfItems2() {
      if ($('#shop-item-page__item-slider li').length > 1) {
        if (j == 0) {
          j++;
          document.lightsliderOnItem2.refresh();
          $('.second-page-slider').css('visibility', 'visible');
        }
      } else {
        $('.second-page-slider').css('visibility', 'visible');
      }
  }
  /*change rows container*/
  function changeRowsContainer2() {
    $('.second-page-slider > .lSAction').remove();
    $('.lSAction').css('opacity', 1);
    $('.second-page-slider .lSAction').appendTo('.second-page-slider');
  }
}
