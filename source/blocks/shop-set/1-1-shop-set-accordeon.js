//!! work only with backend
var accordeonEvent = '';

//$(document).ready(function() {
/* ^> on sets accordeon */
$('#accordionSets .collapse').on('show.bs.collapse', function(event) {
  $('body').css('overflow', 'hidden');
  $(event.target).parent().find('.faq-arrow-new').addClass('faq-arrow-look-up');
  // setTimeout(function() {
  //   itemSlider2();
  // }, 350);
  accordeonEvent = event;
});
$('#accordionSets .collapse').on('hide.bs.collapse', function(event) {
  $('body').css('overflow', 'hidden');
  $('#accordionSets .card-body-block-wrapper').css('opacity', '0');
  var loader = $('.card-body .loader');
  if(loader) {
    loader.removeClass('hidden');
  }
  setTimeout(function() {
    if(loader) {
    loader.removeClass('visuallyhidden');
    }
  }, 20);
  if($(event.target).parent().find('.faq-arrow-new')){
    $(event.target).parent().find('.faq-arrow-new').removeClass('faq-arrow-look-up');
  }
  if (document.lightsliderOnItem2) {
    setTimeout(function() {
      document.lightsliderOnItem2.destroy();
    }, 200);
  }
});
//});
function dataForAccordeonIsHere() {
  $(accordeonEvent.target).parent().find('.card-body-block-wrapper').css({ opacity: '1', height: '100%' });
  setTimeout(function() {
    var loader = $(accordeonEvent.target).parent().find('.card-body .loader');
    loader.addClass('visuallyhidden');
    loader.one('transitionend', function(e) {
      loader.addClass('hidden');
    });
  }, 1000);
}
