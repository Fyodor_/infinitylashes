$(document).ready(function () {
  /*cookies*/
  setTimeout(
    function () {
      $('.cookies-wrapper').css('bottom', '0');
    },
    100000
  )
  $('body').on('click', '.close-cookies', function (event) {
    $('.cookies-wrapper').css('bottom', '-100%');
    //event.preventDefault();
  });

  /*subscribe - commented because moved to backend*/
  //$('body').on('click', '.footer-subscribe', function (event) {
  //  $('#subscribeSaccess').css('bottom', '0');
  //  setTimeout(
  //    function () {
  //      $('#subscribeSaccess').css('bottom', '-100%');
  //    },
  //    5000
  //  )
  //  //event.preventDefault();
  //});
  $('body').on('click', '.close-subscribe', function (event) {
    $('#footer-popup').css('bottom', '-100%');
    //event.preventDefault();
  });

  /* +- on footer accordeon */
  $('.collapse').on('shown.bs.collapse', function (event) {
    $(event.target).parent().find('button').addClass('footer__two-accordeon-change-plus');
  });
  $('.collapse').on('hidden.bs.collapse', function (event) {
    $(event.target).parent().find('button').removeClass('footer__two-accordeon-change-plus');
  });

});