$(document).ready(function () {
  /* ============================================================================
   * главная - обрежем лишний текст в карточке курсов и поставим многоточие
   * ============================================================================ */
  $(".card-text").text(function (i, text) {
    var lenthOfText = 75;
    if (text.length >= lenthOfText) {
      text = text.substring(0, lenthOfText);
      var lastIndex = text.lastIndexOf(" "); // позиция последнего пробела
      text = text.substring(0, lastIndex) + '...'; // обрезаем до последнего слова
    }
    $(this).text(text);
  });
  /* ============================================================================
   * главная - делаем слайдер из карточек курсов и статей при меньших разрешениях
   * ============================================================================ */
  $(window).resize(function () {
    if ($(window).width() < 992) {
      fromCardsToCarousel();
    }
  });
  if ($(window).width() < 992) {
    fromCardsToCarousel();
  }
  $(window).resize(function () {
    if ($(window).width() >= 992) {
      fromCarouseltoCards();
    }
  });
  if ($(window).width() >= 992) {
    fromCarouseltoCards();
  }

  function fromCardsToCarousel() {
    //console.log('<992');
    $('.main-page .courses .items').css('display', 'none');
    $('.main-page .courses .owl-carousel-main-cards').css('display', 'block');
    $('.owl-carousel-main-cards').owlCarousel({
      loop: false,
      margin: 30,
      nav: false,
      dots: true,
      //autoplay: true,
      responsive: {
        0: {
          items: 1
        },
        576: {
          items: 1
        },
        767: {
          items: 2
        },
      }
    });

    $('.main-page .latest-article .latest-article__items').css('display', 'none');
    $('.main-page .latest-article .owl-carousel-latest-article__items').css('display', 'block');
    $('.owl-carousel-latest-article__items').owlCarousel({
      loop: false,
      margin: 30,
      nav: false,
      dots: true,
      //autoplay: true,
      responsive: {
        0: {
          items: 1
        },
        576: {
          items: 1
        },
        767: {
          items: 2
        },
      }
    });
  }

  function fromCarouseltoCards() {
    //console.log('>992');
    $('.main-page .courses .items').css('display', 'block');
    $('.main-page .courses .owl-carousel-main-cards').css('display', 'none');

    $('.main-page .latest-article .latest-article__items').css('display', 'flex');
    $('.main-page .latest-article .owl-carousel-latest-article__items').css('display', 'none');
  }
});