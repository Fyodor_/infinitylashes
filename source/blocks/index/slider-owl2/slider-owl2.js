/* ============================================================================
 * first slider on main page
 * ============================================================================ */
$(document).ready(function () {
  $('.owl-carousel__first-on-index-page').owlCarousel({
    loop: false,
    margin: 0,
    nav: false,
    //autoplay: true,
    smartSpeed: 1500,
    //mouseDrag: false,
    //touchDrag: false,
    //autoplaySpeed: 5000,
    //animateOut: 'fadeOut',
    autoplayTimeout: 10000,
    //autoplayHoverPause: true,
    dragEndSpeed: 300,
    rewind: true,
    //slideTransition: "linear",
    responsive: {
      0: {
        items: 1,
        //mouseDrag: false,
        //touchDrag: false,
        loop: false,
        //animateOut: false,
        autoplay: false,
      },
      575: {
        items: 1,
        //mouseDrag: false,
        //touchDrag: false,
        loop: false,
        //animateOut: false,
        autoplay: false,
      },
      1000: {
        items: 1,
        //mouseDrag: false,
        //touchDrag: false,
      }
    }
  })

  //change img size
  $(window).resize(
    function () {
      setImgForMainSlider();
    }
  )

  function setImgForMainSlider() {
    if (window.innerWidth < 600) {
      var src = $('.owl-carousel__first-on-index-page .item');
      $(src).each(
        function () {
          var t = $(this).data('small');
          $(this).css('background', t);
        }
      )
    } else {
      var src = document.querySelectorAll('.owl-carousel__first-on-index-page .item');
      $(src).each(
        function () {
          var t = $(this).data('big');
          $(this).css('background', t);
        }
      )
    }
  }
  setImgForMainSlider();
});