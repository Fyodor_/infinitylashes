$(document).ready(function () {
  /* ============================================================================
   * выезжающий билет снизу
   * ============================================================================ */
  setTimeout(function () {
    $('.course-page__ticket-flow').addClass('course-page__animate-ticket');
  }, 1000);
  /* ============================================================================
   * закрепляющийся билет рядом с основной о нём информацией
   * ============================================================================ */
  if ($('body').hasClass('course-page')) {
    var offset = 400;
    var waypoint = 0;
    $(window).resize( //to change indent in different window sizes
      function () {
        changeOtstup();
      }
    );

    function changeOtstup() {
      if ($(window).width() > 1199) {
        //.course-page__animate-ticket === top 400px
        offset = 400;
      }
      /*else if ($(window).width() < 991) {
             waypoint.destroy();
           }*/
      else {
        offset = 200;
      }
      if (waypoint) {
        waypoint.destroy();
      }
      startWaypoints();
    }

    changeOtstup();

    function startWaypoints() {
      //console.log('waypoints');
      waypoint = new Waypoint({
        element: document.getElementById('course-page__ticket-down'),
        handler: function (direction) {
          if (direction === 'down') {
            $('.course-page__ticket-flow').addClass('course-page__hook');
          } else if (direction === 'up') {
            $('.course-page__ticket-flow').removeClass('course-page__hook');
          }
        },
        offset: offset
      });
    }



    /* ============================================================================
     * disable carousel cycling
     * ============================================================================ */
    //$('#carouselCourse').carousel({
    //interval: false
    //});
  }

  /* ============================================================================
   * если меньше 2х фото на слайдере
   * ============================================================================ */
  if ($('body').hasClass('course-page')) {
    var numOfSlides = $('.course-page__article-slider .carousel-item');
    //console.log(numOfSlides.length);
    if (numOfSlides.length < 2) {
      $('.carousel-indicators, .carousel-control-next, .carousel-control-prev').css('visibility', 'hidden');
    }
  }


  //for static - show second window
  //$('#registrationForTheCourse').on('hidden.bs.modal', function (e) {
  //  $('#thanksForRegistrationForTheCourse').modal('toggle');
  //})

});