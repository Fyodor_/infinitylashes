/* ============================================================================
 * aside menu clicks - show - hide border
 * ============================================================================ */
$(document).ready(function() {
  $('body').on('click', '.card-body a, .card-header a', function(event) {
    $('#accordion').find('a').removeClass('active');
    $(this).addClass('active');
    $('.card').css('border-bottom', 'none');
    //$(event.target).closest('.card').css('border-bottom', '1px solid #d9d9d9');
  });
  $('body').on('click', '.card-header a', function(event) {
    $('#accordion').find('.collapse').removeClass('show');
    $('#accordion').find('button').addClass('collapsed');
    $('.aside-plus-button').html('<svg class="shop-plus-aside svg"><use xlink:href="#shop-plus"></use></svg>');
  });

  /* ============================================================================
   * aside menu - change + -
   * ============================================================================ */
  // $('.aside-plus-button').on('click', function (event) {
  //   $(this).text(function (i, text) {
  //     if ($('.aside-plus-button') !== event.target) {
  //       //$('.aside-plus-button').text('+');
  //       $('.aside-plus-button').html('<svg class="shop-minus-aside svg"><use xlink:href="#shop-minus"></use></svg>');

  //     }
  //     //return text === "+" ? "-" : "+";
  //     return text === '<svg class="shop-plus-aside svg"><use xlink:href="#shop-plus"></use></svg>' ? '-1' : '<svg class="shop-plus-aside svg"><use xlink:href="#shop-plus"></use></svg>';

  //   })
  // });

  $('#accordion').on('show.bs.collapse', function(event) {
    $('#accordion').find('button').html('<svg class="shop-plus-aside svg"><use xlink:href="#shop-plus"></use></svg>');
    $(event.target)
      .parent()
      .find('button')
      .html('<svg class="shop-minus-aside svg"><use xlink:href="#shop-minus"></use></svg>');
  });
  $('#accordion').on('hidden.bs.collapse', function(event) {
    $(event.target)
      .parent()
      .find('button')
      .html('<svg class="shop-plus-aside svg"><use xlink:href="#shop-plus"></use></svg>');
  });
});
/* ============================================================================
 * aside menu - dell + if don`t have subfolders
 * ============================================================================ */
$('.card').each(function(i, elem) {
  if ($(elem).find('.collapse').length === 0) {
    $(elem).find('.btn').addClass('remove-plus');
  }
});
