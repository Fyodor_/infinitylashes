$(document).ready(function () {
  /* ============================================================================
   * show - hide filters
   * ============================================================================ */
  $('body').on('click', '.shop-page__category-adaptive', function () {
    $('.category-adaptive-wrapper').toggleClass("show-adaptive-wrapper");
    //$('.shop-page__category-adaptive').toggleClass("shop-page__category-adaptive-minus");
  });
  //$('body').on('click', '.filter-close', function () {
  //  $('.filters-wrapper').removeClass("show-filters");
  //  $('.shop-page__filter').removeClass("shop-page__filter-minus");
  //});

  $('body').on('click', '.card-body a, .card-header a', function (event) {
    $('#accordion2').find('a').removeClass('active');
    $(this).addClass("active");
    $('.card').css('border-bottom', 'none');
    $(event.target).closest('.card').css('border-bottom', '1px solid #d9d9d9');
  });
  $('body').on('click', '.card-header a', function (event) {
    $('#accordion2').find('.collapse').removeClass('show');
    $('#accordion2').find('button').addClass('collapsed');
    $('.aside-plus-button').text('+');
  });
  /* hide when click outside */
  $(document).mouseup(function (e) { // событие клика по веб-документу
    var div = $(".category-adaptive-wrapper"); // тут указываем ID элемента
    if (!div.is(e.target) // если клик был не по нашему блоку
      &&
      div.has(e.target).length === 0 // и не по его дочерним элементам
      &&
      !$('.shop-page__category-adaptive').is(e.target)
    ) {
      $('.category-adaptive-wrapper').removeClass("show-adaptive-wrapper");
      $('.shop-page__category-adaptive').removeClass("shop-page__category-adaptive-minus");
    }
  });
});