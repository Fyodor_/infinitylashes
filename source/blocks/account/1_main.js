$(document).ready(function() {
  $('body').on('click', '.account-page__add-address-line', function(event) {
    event.preventDefault();
    var newInput =
      '<input class="form-control" id="account-page__address-add-2" style="margin-top: 15px" name="' +
      $(this).data('model-name') +
      '" type="text" placeholder="Kaiserstr. 59" required="">';
    document.getElementById('account-page__address-add-1').insertAdjacentHTML('afterend', newInput);
    $('.account-page__remove-address-line').css('visibility', 'visible');
    $('.account-page__add-address-line').css('visibility', 'hidden');
  });
  $('body').on('click', '.account-page__remove-address-line', function(event) {
    event.preventDefault();
    $('#account-page__address-add-2').remove();
    $('.account-page__remove-address-line').css('visibility', 'hidden');
    $('.account-page__add-address-line').css('visibility', 'visible');
  });
});
