$(document).ready(function() {
  /* ============================================================================
   * show - hide filters
   * ============================================================================ */
  $('body').on('click', '.shop-page__filter', function() {
    $('.filters-wrapper').toggleClass('show-filters');
    $('.shop-page__filter').toggleClass('shop-page__filter-minus');
  });
  $('body').on('click', '.filter-close', function() {
    $('.filters-wrapper').removeClass('show-filters');
    $('.shop-page__filter').removeClass('shop-page__filter-minus');
  });
  /* hide when click outside */
  $(document).mouseup(function(e) {
    // событие клика по веб-документу
    var div = $('.filters-wrapper'); // тут указываем ID элемента
    if (
      !div.is(e.target) && // если клик был не по нашему блоку
      div.has(e.target).length === 0 && // и не по его дочерним элементам
      !$('.shop-page__filter').is(e.target)
    ) {
      $('.filters-wrapper').removeClass('show-filters');
      $('.shop-page__filter').removeClass('shop-page__filter-minus');
    }
  });
  /* ============================================================================
   * change buttons on filters
   * ============================================================================ */
  $('body').on('click', '.filter-1-item, .filter-2-item', function() {
    if ($(this).hasClass('disabled')) {
      return;
    } else {
      if ($(this).hasClass('active')) {
        $(this).removeClass('active');
      } else {
        $(this).parent().find('.filter-1-item, .filter-2-item').removeClass('active');
        $(this).addClass('active');
      }
    }
  });
  $('body').on('click', '.filter-5-item', function() {
    if ($(this).hasClass('disabled-color')) {
      return;
    } else {
      if ($(this).hasClass('active-color')) {
        $(this).removeClass('active-color');
      } else {
        $(this).parent().find('.filter-5-item').removeClass('active-color');
        $(this).addClass('active-color');
      }
    }
  });
  /* ============================================================================
   * change price on filters - nouislider plugin
   * ============================================================================ */
  if ($('body').hasClass('shop-page') || $('body').hasClass('search-page')) {
    if ($('#filter-slider').length) {
      var filterSlider = document.getElementById('filter-slider');

      noUiSlider.create(filterSlider, {
        start: [ 10, 400 ],
        connect: true,
        range: {
          min: 10,
          max: 400
        }
      });

      var inputMin = document.getElementById('min');
      var inputMax = document.getElementById('max');

      filterSlider.noUiSlider.on('update', function(values, handle) {
        var value = values[handle];

        if (handle) {
          //inputMax.value = '$' + Math.round(value);
          inputMax.value = '$' + value;
        } else {
          //inputMin.value = '$' + Math.round(value);
          inputMin.value = '$' + value;
        }
      });

      inputMax.addEventListener(
        'change',
        function() {
          filterSlider.noUiSlider.set([ null, this.value ]);
        },
        false
      );

      inputMin.addEventListener(
        'change',
        function() {
          filterSlider.noUiSlider.set([ this.value, null ]);
        },
        false
      );
    }
  }
});
