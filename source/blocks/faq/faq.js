$(document).ready(function () {
  /* ^> on faq accordeon */
  $('#accordionFaq .collapse').on('show.bs.collapse', function (event) {
    $(event.target).parent().find('.faq-arrow-new').addClass('faq-arrow-look-up');
  });
  $('#accordionFaq .collapse').on('hide.bs.collapse', function (event) {
    $(event.target).parent().find('.faq-arrow-new').removeClass('faq-arrow-look-up');
  });
});