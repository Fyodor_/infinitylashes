/* ============================================================================
 * carousel
 * ============================================================================ */
$(document).ready(function() {
  /*bug with slider on item page*/
  if ($('body').hasClass('shop-item-page') || $('body').hasClass('shop-set-page')) {
    setTimeout(function() {
      allPagesCarousel();
    }, 500);
  } else {
    // other pages
    allPagesCarousel();
  }

  function allPagesCarousel() {
    $('.all-pages-carousel').owlCarousel({
      loop: false,
      margin: 0,
      nav: true,
      dots: false,
      autoplay: false,
      smartSpeed: 1500,
      //autoplaySpeed: 5000,
      autoplayTimeout: 10000,
      autoplayHoverPause: true,
      dragEndSpeed: 300,
      rewind: true,
      responsive: {
        200: {
          items: 1,
          dots: true,
          dotsEach: 2
        },
        575: {
          items: 3,
          dots: true
        },
        767: {
          items: 4,
          dots: true
        },
        992: {
          items: 5
        },
        1200: {
          items: 6
        }
      }
    });
  }
  /* ============================================================================
   * all pages carousel - обрежем лишний текст в карточке курсов и поставим многоточие
   * ============================================================================ */
  $('.all-pages-carousel header').text(function(i, text) {
    var lenthOfText = 45;
    if (text.length >= lenthOfText) {
      text = text.substring(0, lenthOfText);
      var lastIndex = text.lastIndexOf(' '); // позиция последнего пробела
      text = text.substring(0, lastIndex) + '...'; // обрезаем до последнего слова
    }
    $(this).text(text);
  });
});
