$(document).ready(function () {
    /* ============================================================================
     * прокрутка до верха страницы
     * ============================================================================ */
    $("a.scrolltop").click(function () {
        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top;
        jQuery("html:not(:animated),body:not(:animated)").animate({
            scrollTop: destination
        }, 800);
        return false;
    });
});

//добавить ссылке класс scrolltop и href="#exampleInputEmail1" до которого подойти