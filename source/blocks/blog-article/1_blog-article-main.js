$(document).ready(function () {
  if ($('body').hasClass('blog-article-page')) {
    var numOfSlides = $('.carousel-item');
    if (numOfSlides.length < 1) {
      $('.carousel-indicators, .carousel-control-next, .carousel-control-prev').css('visibility', 'hidden');
    }
  }
});