$(document).ready(function () {
  $("body").on('click', '.card-body a', function () {
    jquery_send('main', 'POST', 'php/function.php', ['change'], ['change']);
  });
  //!!!ВЫЗОВ - jquery_send(elemm, 'post', 'function.php', ['param'], [param.value])
  function jquery_send(elemm, method, program, param_arr, value_arr) {
    var str = ''; //!!! - начальный str=''
    for (var i = 0; i < param_arr.length; i++) { //!!! - массивы - перебор
      str += param_arr[i] + '=' + encodeURIComponent(value_arr[i]) + '&'; //!!! - накапливаем str
    }
    $.ajax({
      type: method,
      url: program,
      data: str,
      success: function (data) {
        $(elemm).html(data);
      } //!!! - ВЫВОД В ЭЛЕМЕНТ ТУТ - function(data){$('elemm').html(data);
    });
  }

  /* ============================================================================
   * крточки товаров - обрежем лишний текст в карточке курсов и поставим многоточие
   * ============================================================================ */
  $(".shop-item__item-title").text(function (i, text) {
    var lenthOfText = 45;
    if (text.length >= lenthOfText) {
      text = text.substring(0, lenthOfText);
      var lastIndex = text.lastIndexOf(" "); // позиция последнего пробела
      text = text.substring(0, lastIndex) + '...'; // обрезаем до последнего слова
    }
    $(this).text(text);
  });
});