$(document).ready(function () {
  var searchInput = $('.main-search-null');
  if ($('body').hasClass('search-page')) {
    $(window).resize(
      function () {
        changeAdaptiveSearchNull();
      }
    );
    changeAdaptiveSearchNull();
  }

  function changeAdaptiveSearchNull() {
    if ($(window).width() < 991) {
      $('.parent-for-adaptive-search').prepend(searchInput);
    } else {
      $('.search-page__null-result-block').prepend(searchInput);
    }
  }
});