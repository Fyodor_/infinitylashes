$(document).ready(function() {
  /* ============================================================================
   * checkout page - add address line + remove address line - one line add - remove
   * ============================================================================ */

  $('body').on('click', '.checkout-page__add-address-line', function(event) {
    event.preventDefault();
    var newInput =
      '<input class="form-control" id="checkout-page__address-add-2" style="margin-top: 15px" name="' +
      $(this).data('model-name') +
      '" type="text" placeholder="Kaiserstr. 59" required="">';
    document.getElementById('checkout-page__address-add-1').insertAdjacentHTML('afterend', newInput);
    $('.checkout-page__remove-address-line').css('visibility', 'visible');
    $('.checkout-page__add-address-line').css('visibility', 'hidden');
  });
  $('body').on('click', '.checkout-page__remove-address-line', function(event) {
    event.preventDefault();
    $('#checkout-page__address-add-2').remove();
    $('.checkout-page__remove-address-line').css('visibility', 'hidden');
    $('.checkout-page__add-address-line').css('visibility', 'visible');
  });
  $('body').on('click', '.checkout-page__add-address-line-base', function(event) {
    event.preventDefault();
    var newInput =
      '<input class="form-control" id="checkout-page__address-add-base-2" style="margin-top: 15px" name="' +
      $(this).data('model-name') +
      '" type="text" placeholder="Kaiserstr. 59" required="">';
    document.getElementById('checkout-page__address-add-base-1').insertAdjacentHTML('afterend', newInput);
    $('.checkout-page__remove-address-line-base').css('visibility', 'visible');
    $('.checkout-page__add-address-line-base').css('visibility', 'hidden');
  });
  $('body').on('click', '.checkout-page__remove-address-line-base', function(event) {
    event.preventDefault();
    $('#checkout-page__address-add-base-2').remove();
    $('.checkout-page__remove-address-line-base').css('visibility', 'hidden');
    $('.checkout-page__add-address-line-base').css('visibility', 'visible');
  });
  /* ============================================================================
   * checkbox change block (Use this address for billing)
   * ============================================================================ */
  $('body').on('change', '#checkout-page__member-check', checkoutPage);

  function checkoutPage() {
    if ($('#checkout-page__member-check').prop('checked')) {
      $('.checkout-page__this-adress-checked').removeClass('checkout-page__payment-hide');
      $('.checkout-page__this-adress-unchecked').removeClass('checkout-page__payment-show');
      $('.checkout-page__this-adress-unchecked').addClass('checkout-page__payment-hide');
    } else {
      $('.checkout-page__this-adress-checked').addClass('checkout-page__payment-hide');
      $('.checkout-page__this-adress-unchecked').removeClass('checkout-page__payment-hide');
      $('.checkout-page__this-adress-unchecked').addClass('checkout-page__payment-show');
    }
  }
  checkoutPage();
  /* checkout - change card img on radio change */
  $('input[name=checkout-page__payment-card]').change(function(event) {
    console.log($(event.target).attr('id'));
    switch ($(event.target).attr('id')) {
      case 'checkout-page__payment-card-1':
        $('.checkout-page__form-ready-body-svg-change').html(
          '<svg class="payment-method-card cardyellow svg"><use xlink:href="#cardyellow"></use></svg>'
        );
        break;
      case 'checkout-page__payment-card-pp':
        $('.checkout-page__form-ready-body-svg-change').html(
          '<svg class="payment-method-card payment-method-card-paypal paypal svg"><use xlink:href="#paypal"></use></svg>'
        );
        break;
      case 'checkout-page__payment-card-su':
        $('.checkout-page__form-ready-body-svg-change').html(
          '<svg class="payment-method-card payment-method-card-sofort sofort svg"><use xlink:href="#sofort"></use></svg>'
        );
        break;
      default:
        $('.checkout-page__form-ready-body-svg-change').html(
          '<svg class="payment-method-card cardyellow svg"><use xlink:href="#cardyellow"></use></svg>'
        );
    }
  });
  /* custom tooltip for where is my code*/
  // $('.checkout-page__where-code span').on('click', function () {
  //   $('.checkout-page__where-code-popup').toggleClass('checkout-page__where-code-popup-show');
  // });
  /* changed for back for ajax */
  $(document).on('click', '.checkout-page__where-code span', function() {
    $(document).find('.checkout-page__where-code-popup').toggleClass('checkout-page__where-code-popup-show');
  });
  $('body').on('click', function(event) {
    if ($(event.target).attr('id') !== 'where-is-code') {
      if ($('.checkout-page__where-code-popup').hasClass('checkout-page__where-code-popup-show')) {
        $('.checkout-page__where-code-popup').removeClass('checkout-page__where-code-popup-show');
      }
    }
  });
});
