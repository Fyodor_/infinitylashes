$(document).ready(function () {
  /* ============================================================================
   * дополнение к анимации инпутов
   * ============================================================================ */
  $(".input-effect input, .input-effect textarea").val("");

  $(".input-effect input, .input-effect textarea").focusout(function () {
    if ($(this).val() != "") {
      $(this).addClass("has-content");
    } else {
      $(this).removeClass("has-content");
    }
  });
});

/* ==================================
 * выбор файлов и отображение имён
 * ====================================*/
// if ($('body').hasClass('contact-us-page')) {
//   $('#load-file-contacts').on('change', function (e) {
//     handleChange(this);
//   })

//   const Store = {
//     files: [], // какое-то хранилище файлов, для примера так
//   }
//   // при выборе файлов, мы будем их добавлять
//   function handleChange(e) {
//     // если не выбрали файл и нажали отмену, то ничего не делать
//     if (!e.files.length) {
//       return;
//     }
//     // создаем новый массив с нашими файлами
//     const files = Object.keys(e.files).map((i) => e.files[i]);
//     addFiles(files); //добавляем файлы в хранилище
//     // очищаем input, т.к. файл мы сохранили
//     e.value = '';
//     showFiles();
//   }

//   function addFiles(files) {
//     // добавляем файлы в общую кучу
//     Store.files = Store.files.concat(files);
//   }
//   // удалить файл из хранилища, например по индексу в массиве
//   function removeFile(index) {
//     Store.files.splice(index, 1);
//     showFiles();
//   }
//   // если надо послать файлы на сервер, формируем FormData с файлами
//   // const formData = getFilesFormData(Store.files);
//   function getFilesFormData() {
//     var formData = new FormData();
//     $.each(Store.files, function (i, v) {
//       formData.append(`file${i + 1}`, v);
//     });
//     return formData;
//   }

//   function showFiles() {
//     var files = '';
//     $.each(Store.files, function (i, v) {
//       files += '<p class="files-added">' + ' <a href="javascript:void(0)" onclick="removeFile(' + i + ')"><svg class="close-added-file svg"><use xlink:href="#close"></use></svg></a>' + v.name + '</p>';
//     });
//     $('#filesAdded').html(files);
//   }
// }

if ($('body').hasClass('contact-us-page')) {
  var handleChange = function handleChange(e) {
    // если не выбрали файл и нажали отмену, то ничего не делать
    if (!e.files.length) {
      return;
    }
    // создаем новый массив с нашими файлами
    var files = Object.keys(e.files).map(function (i) {
      return e.files[i];
    });
    addFiles(files); //добавляем файлы в хранилище
    // очищаем input, т.к. файл мы сохранили
    e.value = '';
    showFiles();
  };

  var addFiles = function addFiles(files) {
    // добавляем файлы в общую кучу
    Store.files = Store.files.concat(files);
  };
  // удалить файл из хранилища, например по индексу в массиве


  var removeFile = function removeFile(index) {
    Store.files.splice(index, 1);
    showFiles();
  };
  // если надо послать файлы на сервер, формируем FormData с файлами
  // const formData = getFilesFormData(Store.files);


  var getFilesFormData = function getFilesFormData() {
    var formData = new FormData();
    $.each(Store.files, function (i, v) {
      formData.append('file' + (i + 1), v);
    });
    return formData;
  };

  var showFiles = function showFiles() {
    var files = '';
    $.each(Store.files, function (i, v) {
      files += '<p class="files-added">' + ' <a href="javascript:void(0)" onclick="removeFile(' + i + ')"><svg class="close-added-file svg"><use xlink:href="#close"></use></svg></a>' + v.name + '</p>';
    });
    $('#filesAdded').html(files);
  };

  $('#load-file-contacts').on('change', function (e) {
    handleChange(this);
  });

  var Store = {
    files: [] // какое-то хранилище файлов, для примера так

    // при выборе файлов, мы будем их добавлять
  };
}