$(document).ready(function () {
  // $('input[type="file"]').on('change', function (event, files, label) {
  //   var file_name = this.value.replace(/\\/g, '/').replace(/.*\//, '')
  //   //$('.filename').text(file_name);
  //   console.log(file_name);
  // });

});

/* ==================================
 * выбор файлов и отображение имён
 * ====================================*/
// var target = '';
// $('#loadFileTrain, #loadFileDist').on('change', function (e) {
//   target = e.target.id;
//   handleChange(this);
// })

// const Store = {
//   files: [], // какое-то хранилище файлов, для примера так
// }
// const StoreTwo = {
//   files: [], // какое-то хранилище файлов 2
// }
// // при выборе файлов, мы будем их добавлять
// function handleChange(e) {
//   // если не выбрали файл и нажали отмену, то ничего не делать
//   if (!e.files.length) {
//     return;
//   }
//   // создаем новый массив с нашими файлами
//   switch (target) {
//     case 'loadFileTrain':
//       const files = Object.keys(e.files).map((i) => e.files[i]);
//       addFiles(files); //добавляем файлы в хранилище
//       break;
//     case 'loadFileDist':
//       const files2 = Object.keys(e.files).map((i) => e.files[i]);
//       addFiles(files2); //добавляем файлы в хранилище
//       break;
//   }
//   // очищаем input, т.к. файл мы сохранили
//   e.value = '';
//   showFiles();
// }

// function addFiles(files) {
//   switch (target) {
//     case 'loadFileTrain':
//       // добавляем файлы в общую кучу
//       Store.files = Store.files.concat(files);
//       break;
//     case 'loadFileDist':
//       StoreTwo.files = StoreTwo.files.concat(files);
//       break;
//   }
// }
// // удалить файл из хранилища, например по индексу в массиве
// function removeFile(index) {
//   StoreTwo.files.splice(index, 1);
//   Store.files.splice(index, 1);
//   showFiles();
// }
// // если надо послать файлы на сервер, формируем FormData с файлами
// // const formData = getFilesFormData(Store.files);
// function getFilesFormData() {
//   switch (target) {
//     case 'loadFileTrain':
//       var formData = new FormData();
//       $.each(Store.files, function (i, v) {
//         formData.append(`file${i + 1}`, v);
//       });
//       return formData;
//       break;
//     case 'loadFileDist':
//       var formData = new FormData();
//       $.each(StoreTwo.files, function (i, v) {
//         formData.append(`file${i + 1}`, v);
//       });
//       return formData;

//   }
// }

// function showFiles() {
//   var files = '';

//   switch (target) {
//     case 'loadFileTrain':
//       $.each(Store.files, function (i, v) {
//         files += '<p class="files-added">' + ' <a href="javascript:void(0)" onclick="removeFile(' + i + ')"><svg class="close-added-file svg"><use xlink:href="#close"></use></svg></a>' + v.name + '</p>';
//       });
//       $('#files').html(files);
//       break;
//     case 'loadFileDist':
//       $.each(StoreTwo.files, function (i, v) {
//         files += '<p class="files-added">' + ' <a href="javascript:void(0)" onclick="removeFile(' + i + ')"><svg class="close-added-file svg"><use xlink:href="#close"></use></svg></a>' + v.name + '</p>';
//       });
//       $('#files2').html(files);
//       break;
//   }
// }

'use strict';

var target = '';
$('#loadFileTrain, #loadFileDist').on('change', function (e) {
  target = e.target.id;
  handleChange(this);
});

var Store = {
  files: [] // какое-то хранилище файлов, для примера так
};
var StoreTwo = {
  files: [] // какое-то хранилище файлов 2

  // при выборе файлов, мы будем их добавлять
};function handleChange(e) {
  // если не выбрали файл и нажали отмену, то ничего не делать
  if (!e.files.length) {
    return;
  }
  // создаем новый массив с нашими файлами
  switch (target) {
    case 'loadFileTrain':
      var files = Object.keys(e.files).map(function (i) {
        return e.files[i];
      });
      addFiles(files); //добавляем файлы в хранилище
      break;
    case 'loadFileDist':
      var files2 = Object.keys(e.files).map(function (i) {
        return e.files[i];
      });
      addFiles(files2); //добавляем файлы в хранилище
      break;
  }
  // очищаем input, т.к. файл мы сохранили
  e.value = '';
  showFiles();
}

function addFiles(files) {
  switch (target) {
    case 'loadFileTrain':
      // добавляем файлы в общую кучу
      Store.files = Store.files.concat(files);
      break;
    case 'loadFileDist':
      StoreTwo.files = StoreTwo.files.concat(files);
      break;
  }
}
// удалить файл из хранилища, например по индексу в массиве
function removeFile(index) {
  StoreTwo.files.splice(index, 1);
  Store.files.splice(index, 1);
  showFiles();
}
// если надо послать файлы на сервер, формируем FormData с файлами
// const formData = getFilesFormData(Store.files);
function getFilesFormData() {
  switch (target) {
    case 'loadFileTrain':
      var formData = new FormData();
      $.each(Store.files, function (i, v) {
        formData.append('file' + (i + 1), v);
      });
      return formData;
      break;
    case 'loadFileDist':
      var formData = new FormData();
      $.each(StoreTwo.files, function (i, v) {
        formData.append('file' + (i + 1), v);
      });
      return formData;

  }
}

function showFiles() {
  var files = '';

  switch (target) {
    case 'loadFileTrain':
      $.each(Store.files, function (i, v) {
        files += '<p class="files-added">' + ' <a href="javascript:void(0)" onclick="removeFile(' + i + ')"><svg class="close-added-file svg"><use xlink:href="#close"></use></svg></a>' + v.name + '</p>';
      });
      $('#files').html(files);
      break;
    case 'loadFileDist':
      $.each(StoreTwo.files, function (i, v) {
        files += '<p class="files-added">' + ' <a href="javascript:void(0)" onclick="removeFile(' + i + ')"><svg class="close-added-file svg"><use xlink:href="#close"></use></svg></a>' + v.name + '</p>';
      });
      $('#files2').html(files);
      break;
  }
}























// $('#show').click(function (e) {
//   e.preventDefault();
//   var formData = getFilesFormData();
//   for (var value of formData.values()) {
//     console.log(value.size); //50000000
//   }
// });