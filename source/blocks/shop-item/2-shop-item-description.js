$(document).ready(function() {
  /*show - hide item description */
  $('.shop-item-page__product-details h5').click(function() {
    if ($(window).width() <= 991) {
      $('.shop-item-page__description-text-wrapper').slideToggle(400, function() {
        $('.shop-item-page__product-details h5').toggleClass('shop-item-page__text-plus-change');
      });
    }
  });
});
