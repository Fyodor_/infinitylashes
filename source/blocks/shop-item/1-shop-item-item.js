$(document).ready(function() {
  /* ============================================================================
   * shop-item-page - item slider
   * ============================================================================ */
  if ($('body').hasClass('shop-item-page')) {
    // var lightsliderOnItem = 0;

    // function itemSlider() {
    //   var verticalHorizontal = true;
    //   if ($(window).width() < 1199) {
    //     verticalHorizontal = false;
    //   }

    //   $(window).on('resize', function () {
    //     lightsliderOnItem.refresh();
    //   });

    //   //https://github.com/sachinchoolur/lightslider/issues/61
    //   //https://stackoverflow.com/questions/46421110/light-slider-refresh-not-working

    //   lightsliderOnItem = $('#shop-item-page__item-slider').lightSlider({
    //     gallery: true,
    //     item: 1,
    //     vertical: verticalHorizontal,
    //     verticalHeight: 430, //555
    //     vThumbWidth: 91,
    //     thumbItem: 4,
    //     thumbMargin: 10,
    //     slideMargin: 0,
    //     galleryMargin: 30,
    //     adaptiveHeight: true,
    //     responsive: [{
    //         breakpoint: 991,
    //         settings: { // settings for width 480px to 1199px
    //           thumbItem: 0,
    //           gallery: false,
    //           adaptiveHeight: true,
    //         },
    //       },
    //       {
    //         breakpoint: 0,
    //         settings: { // settings for width 0px to 480px
    //           adaptiveHeight: true,
    //           //item: 2,
    //           //slideMove: 1
    //         }
    //       }
    //     ]
    //   });
    // }
    // itemSlider();
    // $(window).resize(function () {
    //   lightsliderOnItem.destroy();
    //   itemSlider();
    // });

    /* переделано для бека, чтобы можно было обратиться */
    if (!document.lightsliderOnItem) {
      document.lightsliderOnItem = 0;
    }

    function itemSlider() {
      var verticalHorizontal = true;
      if ($(window).width() < 1200) {
        verticalHorizontal = false;
      }

      $(window).on('resize', function() {
        document.lightsliderOnItem.refresh();
      });

      //https://github.com/sachinchoolur/lightslider/issues/61
      //https://stackoverflow.com/questions/46421110/light-slider-refresh-not-working
      //$('#shop-item-page__item-slider').goToNextSlide();
      document.lightsliderOnItem = $(document).find('#shop-item-page__item-slider').lightSlider({
        gallery: true,
        item: 1,
        vertical: verticalHorizontal,
        verticalHeight: 430, //555
        vThumbWidth: 100,
        thumbItem: 4,
        thumbMargin: 10,
        slideMargin: 0,
        galleryMargin: 70,
        adaptiveHeight: true,
        loop: true,
        controls: true,
        responsive: [
          {
            breakpoint: 1199,
            settings: {
              thumbItem: 4,
              thumbMargin: 45
            }
          },
          {
            breakpoint: 992,
            settings: {
              thumbItem: 0,
              gallery: false,
              adaptiveHeight: true,
              galleryMargin: 30
            }
          },
          {
            breakpoint: 400,
            settings: {
              verticalHeight: 300,
              gallery: false,
              adaptiveHeight: true
            }
          },
          {
            breakpoint: 0,
            settings: {
              // settings for width 0px to 480px
              adaptiveHeight: true
              //item: 2,
              //slideMove: 1
            }
          }
        ],
        onSliderLoad: function(el) {
          checkNumOfItems();
          changeRowsContainer();
        }
      });
      // $('.shop-item-page__item-slider').css('visibility', 'visible');
    }
    var i = 0;
    itemSlider();
    $(window).resize(function() {
      document.lightsliderOnItem.destroy();
      itemSlider();
    });
    /*fix one image controls hide*/
    function checkNumOfItems() {
      if ($('#shop-item-page__item-slider li').length > 1) {
        if (i == 0) {
          i++;
          document.lightsliderOnItem.refresh();
          $('.shop-item-page__item-slider').css('visibility', 'visible');
        }
      } else {
        $('.shop-item-page__item-slider').css('visibility', 'visible');
      }
    }
    /*change rows container*/
    function changeRowsContainer() {
      //console.log($('.shop-item-page__item-slider .lSSlideWrapper .lSAction').length);
      //if($('.lSSlideWrapper .lSAction').length === 1) {
      $('.shop-item-page__item-slider > .lSAction').remove();
      $('.lSAction').css('opacity', 1);
      $('.lSAction').appendTo('.shop-item-page__item-slider');
      //}
    }

    /* ============================================================================
     * shop-item-page - item zoom
     * ============================================================================ */
    var out4 = $('.shop-item-page__zoom-here');
    if ($(window).width() > 991) {
      $('.shop-item-page__zoom').parent().zoom({
        target: out4,
        magnify: 2,
        onZoomIn: function() {
          $('.shop-item-page__zoom-here').css('z-index', '1');
        },
        onZoomOut: function() {
          $('.shop-item-page__zoom-here').css('z-index', '0');
        }
      });
    }

    /* ============================================================================
     * shop-item-page - stars rating
     * ============================================================================ */
    $(function() {
      $('.star-ratings').barrating({
        theme: 'css-stars',
        readonly: true
        //initialRating: 4
      });
      $('.star-ratings-change').barrating({
        theme: 'css-stars',
        readonly: false
        //initialRating: 4
      });
    });
  }
  /* ============================================================================
   * shop-item-page - quantiti-of-order
   * ============================================================================ */
  $('.shop-item-page .shop-item-page__quantiti-of-order .minus').click(function() {
    var valNow = $('#shop-item-page__quantiti-of-order');
    val = parseInt(valNow.val()) - 1;
    val = val < 1 ? 1 : val;
    valNow.val(val);
    inputPosition();
    return false;
  });
  $('.shop-item-page .shop-item-page__quantiti-of-order .plus').click(function() {
    var valNow = $('#shop-item-page__quantiti-of-order');
    val = parseInt(valNow.val()) + 1;
    valNow.val(val);
    inputPosition();
    //$('.label-wrapper input').css('width', $('.label-wrapper input').val().length + 4* 8 + 'px');
    //console.log($('.label-wrapper input').val().length);
    return false;
  });

  if ($('.label-wrapper').length > 0) {
    inputPosition();
  }

  /* ============================================================================
   * change buttons on shop item
   * ============================================================================ */
  $('body').on('click', '.shop-item-page__choose-block', function(event) {
    if ($(this).hasClass('shop-item-page__choose-block-disabled')) {
      event.preventDefault();
    } else {
      if ($(this).hasClass('shop-item-page__choose-block-active')) {
        $(this).removeClass('shop-item-page__choose-block-active');
        event.preventDefault();
      } else {
        $(this)
          .parent()
          .parent()
          .find('.shop-item-page__choose-block')
          .removeClass('shop-item-page__choose-block-active');
        $(this).addClass('shop-item-page__choose-block-active');
        event.preventDefault();
      }
    }
  });
  /* ============================================================================
   * show cart-hover-menu on add to bag button
   * ============================================================================ */
  $('.shop-item-page__button-for-order a').on('click', function(event) {
    $('.cart-hover-menu').addClass('show-cart-hover-menu');
    setTimeout(function() {
      $('.cart-hover-menu').removeClass('show-cart-hover-menu');
    }, 1500);
    if ($(window).width() < 991) {
      event.preventDefault();
      $('.added-to-cart-wrapper').css('right', '25px');
      setTimeout(function() {
        $('.added-to-cart-wrapper').css('right', '-100%');
      }, 3000);
    }
  });
});

function inputPosition() {
  var inputs = $('.label-wrapper input');
  //console.log(inputs);
  for (var i = 0; i < inputs.length; i++) {
    inputPositionChange(inputs[i]);
  }
}

function inputPositionChange(input) {
  //console.log(input);
  if ($(input).val().length < 2) {
    $(input).css('margin-left', '60%');
  }
  if ($(input).val().length >= 2) {
    $(input).css('margin-left', '55%');
  }
  if ($(input).val().length >= 3) {
    $(input).css('margin-left', '50%');
  }
}
