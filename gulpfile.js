//made by Fyodor Popov -> fyodor.work@gmail.com -> https://github.com/F-y-o-d-o-r/Template

/******************************************************************************/
var gulp = require('gulp'), // Подключаем Gulp
  autoPrefixer = require('gulp-autoprefixer'), // Подключаем библиотеку для автоматического добавления префиксов
  //cssnano = require('gulp-cssnano'), // Подключаем пакет для минификации CSS //сжимаем в другом..
  watch = require('gulp-watch'),
  sourcemaps = require('gulp-sourcemaps'),
  sass = require('gulp-sass'), //Подключаем Sass пакет //_part.sass не войдет, толко через @import 'part' в другом файле sass
  htmlmin = require('gulp-htmlmin'),
  browserSync = require('browser-sync'), // Подключаем Browser Sync
  imageMin = require('gulp-imagemin'), // Подключаем библиотеку для работы с изображениями
  pngquant = require('imagemin-pngquant'), // Подключаем библиотеку для работы с png
  uglify = require('gulp-uglify'), // Подключаем gulp-uglify (для сжатия JS)
  autopolyfiller = require('gulp-autopolyfiller'),
  concat = require('gulp-concat'), //for autopolifiller // Подключаем gulp-concat (для конкатенации файлов)
  order = require('gulp-order'), //for autopolifiller
  merge = require('event-stream').merge, //for autopolifiller
  rigger = require('gulp-rigger'), //Плагин позволяет импортировать один файл в другой простой конструкцией //= footer.html + есть gulp-x-includer  -
  duration = require('gulp-duration'), //время выполнения таска
  del = require('del'), //удалит папку или файл;
  rename = require('gulp-rename'), // Подключаем библиотеку для переименования файлов .pipe(rename({suffix: '.min', prefix : ''}))
  spritesmith = require('gulp.spritesmith'), //делаем спрайты
  pug = require('gulp-pug'),
  sassGlob = require('gulp-sass-glob'), //@import "vars/**/*.scss";    - dir import in sass
  svgSprite = require('gulp-svg-sprites'), //svg - 1 - svg>use xlink:href="#id" (после загрузки инлайново на страницу с дисплей нан). 2 - svg>use xlink:href='/adress/img.svg#id'
  filter = require('gulp-filter'),
  cache = require('gulp-cache'), // Подключаем библиотеку кеширования
  strip = require('gulp-strip-comments'), //remove comments
  notify = require('gulp-notify'), //сообщения в окне браузера об ошибках
  plumber = require('gulp-plumber'), //не выкидывать при ошибках //.pipe(plumber())
  reload = browserSync.reload;
/******************************************************************************/
var path = {
  build: {
    //Тут мы укажем куда складывать готовые после сборки файлы
    html: 'prod',
    js: 'prod/js',
    style: 'prod/css',
    libsstyle: 'source/libs/concat_from_node_modules',
    img: 'prod/img',
    fonts: 'prod/fonts',
    spritesimg: 'source/sprites/ready/',
    //spritesstyles: 'source/sprites/other/styles/',
    spritesstyles: 'source/sprites/ready/',
    spritesSvg: 'source/sprites/ready/',
    //spritesSvgStyles: 'source/libs/svgStyles/',
    spritesSvgStyles: 'source/sprites/ready/'
  },
  source: {
    //Пути откуда брать исходники
    //html: ['source/*.html'], //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
    pug: [ 'source/blocks/*.pug' ], //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .pug
    js: [ 'source/js/main.js', 'source/blocks/**/*.js' ], //В стилях и скриптах нам понадобятся только main файлы
    lib: [
      'source/libs/**/*.js',
      //'node_modules/bootstrap/dist/js/bootstrap.min.js', //bootstrap 4
      'node_modules/popper.js/dist/umd/popper.min.js', //popper for bootstrap
      'node_modules/jquery/dist/jquery.min.js',
      'node_modules/owl.carousel/dist/owl.carousel.min.js',
      'node_modules/waypoints/lib/jquery.waypoints.min.js',
      'node_modules/jquery-zoom/jquery.zoom.min.js',
      'node_modules/jquery-bar-rating/dist/jquery.barrating.min.js',
      'node_modules/nouislider/distribute/nouislider.min.js'
    ], //jquery & libraries
    style: [ 'source/sass/main.sass', 'source/sass/bootstrap.scss' ],
    styleBootstrap: [ 'source/sass/bootstrap.scss' ],
    libsstyles: [
      'source/libs/**/*.css',
      //'node_modules/animate.css/animate.min.css',
      //'node_modules/normalize.css/normalize.css',
      'node_modules/owl.carousel/dist/assets/owl.carousel.min.css',
      'node_modules/owl.carousel/dist/assets/owl.theme.default.min.css',
      //'node_modules/node_modules/slick-carousel/slick/slick.css',
      'node_modules/lightslider/dist/css/lightslider.min.css',
      'node_modules/nouislider/distribute/nouislider.min.css'
    ], //стили библиотек
    libssassstyles: [
      //'node_modules/bootstrap/scss/bootstrap.scss',
    ], //sass библиотек
    img: [ 'source/img/**/*.*' ], //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
    fonts: 'source/fonts/**/*.*',
    sprites: 'source/sprites/other/*.*',
    spritesSvg: 'source/sprites/svg/*.*',
    htacces: 'source/.htacces'
    // [
    // 'app/libs/jquery/dist/jquery.min.js',
    //   'app/js/common.min.js', // Всегда в конце
    // ]
  },
  watch: {
    //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
    //html: 'source/**/*.html',
    pug: [ 'source/blocks/**/*.pug', 'source/standart_blocks/**/*.pug' ],
    js: [ 'source/blocks/**/*.js', 'source/js/main.js', 'source/standart_blocks/**/*.js' ],
    style: [
      'source/blocks/**/*.scss',
      'source/blocks/**/*.sass',
      'source/sass/main.sass',
      'source/standart_blocks/**/*.scss',
      'source/sass/**/*.scss',
      'source/sass/**/*.sass'
    ],
    img: 'source/img/**/*.*',
    fonts: 'source/fonts/**/*.*',
    lib: 'source/libs/**/*.js' //jquery & libraries
  },
  dell: {
    prod: [
      'prod',
      'source/sprites/other/styles',
      'source/libs/svgStyles',
      'source/img/sprites',
      'source/libs/concat_from_node_modules',
      'source/sprites/ready'
    ]
  }
};
//Создадим переменную с настройками нашего dev сервера:
var config = {
  server: {
    baseDir: './prod' // Директория для сервера
  },
  //tunnel: true,
  //tunnel: "fyodor", //Demonstration page: https://fyodor.localtunnel.me //http://myprogect.localtunnel.me//https://localtunnel.github.io/www/
  host: 'localhost',
  port: 8080, //63341, //9000,//8080
  logPrefix: 'Frontend',
  notify: false, // Отключаем уведомления
  open: true
};

/******************************************************************************/
// собираем html - PUG
gulp.task('pug', function() {
  gulp
    .src(path.source.pug)
    .pipe(
      plumber({
        errorHandler: notify.onError('Error: <%= error.message %>')
      })
    )
    .pipe(
      pug({
        pretty: true //min
      })
    )
    .pipe(strip())
    .pipe(duration('pug'))
    .pipe(gulp.dest(path.build.html))
    .pipe(
      reload({
        stream: true
      })
    );
});
/******************************************************************************/
//Собираем стили
gulp.task('style', function() {
  gulp
    .src(path.source.style) //Выберем наш main.scss
    .pipe(
      plumber({
        errorHandler: notify.onError('Error: <%= error.message %>')
      })
    )
    //.pipe(notify("Hello Gulp!"))
    //.pipe(notify("Found file: <%= file.relative %>!"))
    //.pipe(sourcemaps.init())
    .pipe(sassGlob()) //@import "vars/**/*.scss";   - dir import in sass
    //.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(
      sass({
        outputStyle: 'expanded'
      }).on('error', sass.logError)
    )
    .pipe(
      autoPrefixer([ 'last 15 versions', '> 1%', 'ie 8', 'ie 7' ], {
        cascade: true
      })
    ) //Добавим вендорные префиксы
    //.pipe(sourcemaps.write())
    .pipe(
      rename({
        suffix: '.min',
        prefix: ''
      })
    )
    .pipe(duration('css'))
    .pipe(gulp.dest(path.build.style)) //И в build
    .pipe(
      reload({
        stream: true
      })
    );
});
/******************************************************************************/
//Собираем стили bootstrap
gulp.task('styleBootstrap', function() {
  gulp
    .src(path.source.styleBootstrap) //Выберем наш main.scss
    //.pipe(sourcemaps.init())
    .pipe(sassGlob()) //@import "vars/**/*.scss";   - dir import in sass
    .pipe(
      sass({
        outputStyle: 'compressed'
      }).on('error', sass.logError)
    )
    .pipe(
      autoPrefixer([ 'last 15 versions', '> 1%', 'ie 8', 'ie 7' ], {
        cascade: true
      })
    ) //Добавим вендорные префиксы
    //.pipe(sourcemaps.write())
    .pipe(
      rename({
        suffix: '.min',
        prefix: ''
      })
    )
    .pipe(duration('cssBootstrap'))
    .pipe(gulp.dest(path.build.style)) //И в build
    .pipe(
      reload({
        stream: true
      })
    );
});
/******************************************************************************/
//Таск по сборке скриптов js + полифилы
//gulp.task
gulp.task('js', function() {
  // Concat all required js files
  var all = gulp
    .src(path.source.js) //Найдем наш main файл
    .pipe(
      plumber({
        errorHandler: notify.onError('Error: <%= error.message %>')
      })
    )
    .pipe(rigger()) //добавляем присоеденённое по ссылкам
    .pipe(concat('all.js'));
  // Generate polyfills for all files
  var polyfills = all.pipe(autopolyfiller('polyfills.js'), {
    browsers: [ 'last 5 versions', '> 1%', 'ie10' ]
  });
  // Merge polyfills and all files streams
  return (merge(polyfills, all)
      //return merge(all)
      // Order files. NB! polyfills MUST be first
      .pipe(order([ 'polyfills.js', 'all.js' ]))
      // Make single file
      .pipe(concat('main.min.js'))
      //.pipe(sourcemaps.init()) //Инициализируем sourcemap
      //.pipe(uglify())// сжимаем
      //.pipe(sourcemaps.write()) //Пропишем карты
      // And finally write `all.min.js` into `build/` dir
      //.pipe(strip( //remove comments
      //{safe: false}
      //\/\*\s=(.|\n)*?=\s\*\/
      //))
      .pipe(duration('js'))
      .pipe(gulp.dest(path.build.js)) //Выплюнем готовый файл в build
      .pipe(
        reload({
          stream: true
        })
      ) ); //И перезагрузим сервер
});
/******************************************************************************/
//переносим библиотеки - sass в один файл css
gulp.task('lib-sass', function() {
  gulp
    .src(path.source.libssassstyles) //Выберем наши стили библиотек
    //.pipe(sourcemaps.init())
    .pipe(sassGlob()) //@import "vars/**/*.scss";   - dir import in sass
    .pipe(
      sass({
        outputStyle: 'compressed'
      }).on('error', sass.logError)
    )
    .pipe(
      autoPrefixer([ 'last 15 versions', '> 1%', 'ie 8', 'ie 7' ], {
        cascade: true
      })
    ) //Добавим вендорные префиксы
    //.pipe(sourcemaps.write())
    .pipe(
      rename({
        suffix: '.min',
        prefix: ''
      })
    )
    .pipe(duration('lib-sass'))
    .pipe(gulp.dest(path.build.libsstyle)) //
    .pipe(
      reload({
        stream: true
      })
    );
});
//переносим библиотеки - css в один файл - переношу в css
gulp.task(
  'lib-css',
  /*['lib-sass'],*/ function() {
    gulp
      .src(path.source.libsstyles) //Найдем наши файлы
      .pipe(concat('libs.min.css'))
      .pipe(
        sass({
          outputStyle: 'compressed'
        }).on('error', sass.logError)
      ) //min
      .pipe(gulp.dest(path.build.style)) //Выплюнем готовый файл в build
      .pipe(duration('lib-css'))
      .pipe(
        reload({
          stream: true
        })
      ); //И перезагрузим сервер
  }
);
//переносим библиотеки - js
gulp.task('lib', function() {
  gulp
    .src(path.source.lib) //Найдем наш файл
    .pipe(gulp.dest(path.build.js)) //Выплюнем готовый файл в build
    .pipe(duration('lib'))
    .pipe(
      reload({
        stream: true
      })
    ); //И перезагрузим сервер
});
/******************************************************************************/
//Таск по картинкам
gulp.task('image', function() {
  gulp
    .src(path.source.img) //Выберем наши картинки
    .pipe(
      cache(
        imageMin({
          //Сожмем их
          progressive: true,
          svgoPlugins: [
            {
              removeViewBox: false
            }
          ],
          use: [ pngquant() ],
          interlaced: true
        })
      )
    )
    .pipe(gulp.dest(path.build.img)) //И бросим в build
    .pipe(duration('image'))
    .pipe(
      reload({
        stream: true
      })
    );
});
/******************************************************************************/
//Таск по спрайтам
gulp.task('sprite', function() {
  var spriteData = gulp.src(path.source.sprites).pipe(
    spritesmith({
      imgName: 'sprite.png',
      //cssName: 'sprite.sass',
      cssName: 'sprite.css'
    })
  );
  //return spriteData.pipe(gulp.dest('source/img/sprites/'));
  spriteData.img.pipe(gulp.dest(path.build.spritesimg)); // путь, куда сохраняем картинку
  spriteData.css.pipe(gulp.dest(path.build.spritesstyles)); // путь, куда сохраняем стили
});
//Таск по svg спрайтам
gulp.task('spriteSvg', function() {
  return (gulp
      .src(path.source.spritesSvg)
      .pipe(
        svgSprite({
          //shape: {     // Set maximum dimensions
          //dimension: {
          //maxWidth: 500,
          //maxHeight: 500
          //},
          //spacing: {         // Add padding
          //padding: 0
          //}
          //},
          mode: 'symbols'
        })
      )
      //.pipe(gulp.dest(path.build.spritesSvgStyles))
      //.pipe(filter("**/*.svg"))  // Filter out everything except the SVG file
      .pipe(duration('spriteSvg'))
      .pipe(gulp.dest(path.build.spritesSvg)) );
});
/******************************************************************************/
//Шрифты просто копируем
gulp.task('fonts', function() {
  gulp.src(path.source.fonts).pipe(duration('fonts')).pipe(gulp.dest(path.build.fonts));
});
/******************************************************************************/
//.htacces просто копируем
gulp.task('htacces', function() {
  gulp.src(path.source.htacces).pipe(duration('htacces')).pipe(gulp.dest('prod/'));
});
/******************************************************************************/
//php просто копируем
gulp.task('php', function() {
  gulp.src('source/php/*.php').pipe(duration('php')).pipe(gulp.dest('prod/php'));
});
/******************************************************************************/
//очистим кеш
gulp.task('clear', function() {
  return cache.clearAll();
});
//удалим папку prod
gulp.task('del', [ 'clear' ], function() {
  return del.sync(path.dell.prod);
});
/******************************************************************************/
//таск с именем «build», который будет запускать все
gulp.task('build', [
  //'html',
  'pug',
  'js',
  'lib-sass',
  'lib-css',
  'lib',
  'style',
  'styleBootstrap',
  'fonts',
  'htacces',
  'sprite',
  'spriteSvg',
  'image',
  'php'
]);
//таск с именем «work», который будет запускать то что нужно во время работы
gulp.task('work', [
  //'html',
  'pug',
  'js',
  'style'
]);
/******************************************************************************/
//Изменения файлов все
gulp.task('watch', function() {
  //watch([path.watch.html, 'source/blocks/**/*.html'], function (event, cb) {
  //  gulp.start('html');
  //});
  watch(path.watch.pug, function(event, cb) {
    gulp.start('pug');
  });
  watch(path.watch.style, function(event, cb) {
    gulp.start('style');
  });
  watch(path.watch.js, function(event, cb) {
    gulp.start('js');
  });
  watch([ path.watch.lib ], function(event, cb) {
    gulp.start('lib');
  });
  watch([ path.watch.img ], function(event, cb) {
    gulp.start('image');
  });
  /*watch('source/sprites/!*.*', function (event, cb) {
      gulp.start('sprite');
  });*/
  watch([ path.watch.fonts ], function(event, cb) {
    gulp.start('fonts');
  });
  watch('source/php/*.php', function(event, cb) {
    gulp.start('php');
  });
});
//Изменения файлов для work
gulp.task('watch_work', function() {
  //watch([path.watch.html, 'source/blocks/**/*.*'], function (event, cb) {
  //  gulp.start('html');
  //});
  watch(path.watch.pug, function(event, cb) {
    gulp.start('pug');
  });
  watch(path.watch.style, function(event, cb) {
    gulp.start('style');
  });
  watch(path.watch.js, function(event, cb) {
    gulp.start('js');
  });
});
/******************************************************************************/
//Веб сервер// запустим livereload сервер с настройками, которые мы определили в объекте config
gulp.task('webserver', function() {
  browserSync(config);
});
/******************************************************************************/
gulp.task('default', [ 'build', 'webserver', 'watch' ]);
gulp.task('one', [ 'work', 'webserver', 'watch_work' ]);
